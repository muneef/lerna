import React from "react";
import "@chaliyar/tokens/dist/css/_variables.css";

import { Button as SourceButton } from "ariakit/button";

import styled from "@emotion/styled";

/**
 * Add styles for Plate
 */

const BaseButton = styled(SourceButton)`
  box-sizing: border-box;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 4px 12px 4px 8px;
  background: var(--aq-colors-green-900);
  border: 1.5px solid #00471a;
  border-radius: 4px;
  &:hover {
    color: white;
  }
`;

interface ButtonProps {
  /**
   * Is this the principal call to action on the page?
   */
  primary?: boolean;
  /**
   * What background color to use
   */
  backgroundColor?: string;
  /**
   * How large should the button be?
   */
  size?: "small" | "medium" | "large";
  /**
   * Button contents
   */
  label: string;
  /**
   * Optional click handler
   */
  onClick?: () => void;
}

/**
 * Primary UI component for user interaction
 */
export const Button = ({
  primary = false,
  size = "medium",
  backgroundColor,
  label,
  ...props
}: ButtonProps) => {
  const mode = primary ? "btn--primary" : "btn--secondary";
  return (
    <BaseButton
      type="button"
      className={["btn", `btn--${size}`, mode].join(" ")}
      style={{ backgroundColor }}
      {...props}
    >
      {label}
    </BaseButton>
  );
};
