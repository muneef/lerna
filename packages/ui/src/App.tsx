import React from 'react';
import './App.css';

import Button from './components/button';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        Plate Components List
      </header>
    </div>
  );
}

export default App;
